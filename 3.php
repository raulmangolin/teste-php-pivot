<?php
	class Foo {
	    public function __construct($n)
	    {
	        $this->n = $n;
	    }

	    public $n;
	}

	$arrObj = array(
	    new Foo(5),
	    new Foo(12),
	    new Foo(9),
	    new Foo('banana')
	);

	$arrArr = array(
	    0 => array(
	        'n' => 10
	    ),
	    1 => array(
	        'n' => 6
	    ),
	    2 => array(
	        'n' => 8
	    ),
	    3 => array(
	        'n' => 3
	    )
	);
	echo '<pre>';

	$array = array();
	for ($i=0; $i < $total=count($arrObj); $i++) { 
		$array[] = get_object_vars($arrObj[$i]);
	}

	$array = array_merge($array, $arrArr);


	$arrayFinal = array();
	for ($i=0; $i < $total=count($array); $i++) { 
		if (is_numeric($array[$i]['n'])) {
			$arrayFinal[$array[$i]['n']] = $array[$i];
		}
	}
	
	ksort($arrayFinal);
	
	var_dump($arrayFinal);